﻿
namespace Pretest03
{
    public class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("");
                Console.WriteLine("Pretest 3");
                Console.WriteLine("1.Soal 7");
                Console.WriteLine("2.Soal 8");
                Console.WriteLine("3.Soal 9");
                Console.WriteLine("0.exit");
                Console.Write("Pilih : ");
                int pilih = int.Parse(Console.ReadLine());
                switch (pilih)
                {
                    case 0: break;
                    case 1: Soal1(); break;
                    case 2: Soal2(); break;
                    case 3: Soal3(); break;
                    default: Console.WriteLine("Pilih 1-10"); break;
                }

                if (pilih == 0) { break; }
                else
                {
                    Console.WriteLine();
                    Console.Write("apakah anda ingin megulang Y/N ? ");
                    String pilih2 = Console.ReadLine();
                    if (pilih2.Equals("y".ToLower())) { Console.WriteLine(); continue; }
                    else { Console.WriteLine(); break; }
                }
            }
        }

        public static void Soal1()
        {
            Console.WriteLine("== Soal 7 ==");
            Console.WriteLine("1 eskrim = 100");
            Console.WriteLine("Tukarkan 6 stik eskrim dengan 1 eskrim");
            Console.Write("Duit : ");
            int duit = int.Parse(Console.ReadLine());
            int eskrim = 0;
            int stik = 0;
            while (duit >= 100)
            {
                duit -= 100; stik++; eskrim++;
                if (stik == 6) { eskrim++; stik = 1; }
            }
            Console.WriteLine($"eskrim : {eskrim}");
        }

        public static void Soal2()
        {
            Console.WriteLine("== Soal 8 ==");
            Console.Write("input : ");
            char[] kata = Console.ReadLine().ToLower().ToCharArray();
            String huruf = "abcdefghijklmnopqrstuvwxyz";
            String vocal = "aiueo";
            int hvocal = 0; int hkonsonan = 0;
            for (int i = 0; i < kata.Length; i++)
            {
                int nilai = huruf.IndexOf(kata[i])+1;
                if(huruf.IndexOf(kata[i])== vocal.IndexOf(kata[i]) || huruf.IndexOf(kata[i])>0 && vocal.IndexOf(kata[i])>0)
                {
                    hvocal += nilai;
                    //Console.WriteLine(nilai+" vocal");
                }
                else { hkonsonan += nilai; //Console.WriteLine(nilai+" konsonan"); 
                }
                
            }
            int hasil = 0;
            if (hvocal > hkonsonan) { hasil = hvocal - hkonsonan; } else { hasil = hkonsonan - hvocal; }
            Console.WriteLine($"vocal {hvocal}, konsonan {hkonsonan}, selisih {hasil}");
        }
        public static void Soal3()
        {
            Console.WriteLine("== Soal 9 ==");
            Console.WriteLine("naik turun gunung");
            Console.Write("deret : ");
            string[] str1 = Console.ReadLine().ToLower().Split(" ");
            int[] array1 = Array.ConvertAll<String, int>(str1, int.Parse);

            int naik = 0; int turun = 0; int counter = 0; int nTamp = 0;
            for(int i=0; i<array1.Length; i++)
            {
                if (i==0 && array1[i] < array1[i+1]) { naik++; nTamp = 1; } else if(i==0 && array1[i] > array1[i+1]) { turun++; nTamp = -1; }
                else if (i!=array1.Length-1 && array1[i] < array1[i+1] && nTamp < 0) { naik++; nTamp = 1; }else if(i != array1.Length - 1 && array1[i] > array1[i + 1] && nTamp > 0) { turun++; nTamp = -1; }
               
            }

            Console.WriteLine($"naik {naik}, turun {turun}");

            
           
        }
    }
}