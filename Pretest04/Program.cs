﻿
namespace Pretest04
{
    public class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("");
                Console.WriteLine("Pretest 4");
                Console.WriteLine("1.Soal 1");
                Console.WriteLine("2.Soal 2");
                Console.WriteLine("3.Soal 3");
                Console.WriteLine("4.Soal 4");
                Console.WriteLine("5.Soal 5");
                Console.WriteLine("6.Soal 6");
                Console.WriteLine("6.Soal 7");
                Console.WriteLine("8.Soal 8");
                Console.WriteLine("0.exit");
                Console.Write("Pilih : ");
                int pilih = int.Parse(Console.ReadLine());
                switch (pilih)
                {
                    case 0: break;
                    case 1: Soal1(); break;
                    case 2: Soal2(); break;
                    case 3: Soal3(); break;
                    case 4: Soal4(); break;
                    case 5: Soal5(); break;
                    case 6: Soal6(); break;
                    case 7: Soal7(); break;
                    case 8: Soal8(); break;
                    default: Console.WriteLine("Pilih 1-10"); break;
                }

                if (pilih == 0) { break; }
                else
                {
                    Console.WriteLine();
                    Console.Write("apakah anda ingin megulang Y/N ? ");
                    String pilih2 = Console.ReadLine();
                    if (pilih2.Equals("y".ToLower())) { Console.WriteLine(); continue; }
                    else { Console.WriteLine(); break; }
                }
            }
        }

        public static void Soal1()
        {
            Console.WriteLine("== Soal 1 ==");
            Console.Write("Jarak : ");
            int jarak = int.Parse(Console.ReadLine());
            Console.Write("Pindah : ");
            int pindah = int.Parse(Console.ReadLine());
            Console.Write("Deret : ");
            String karakter = Console.ReadLine();
            List<String> list = new List<string>();

            try
            {
                while (karakter.Length > 0)
                {
                    list.Add(karakter.Substring(0, jarak));
                    karakter = karakter.Remove(0, jarak);
                }

                for (int i = 0; i < pindah; i++)
                {
                    String dt = list[list.Count - 1];
                    for (int j = 0; j < list.Count; j++)
                    {

                        if (j == 0)
                        {
                            String nTamp = list[j];
                            list[j] = list[j + 1];
                            list[list.Count - 1] = nTamp;
                        }
                        else if (j == list.Count - 1)
                        {
                            list[j - 1] = dt;
                        }
                        else
                        {
                            String nTamp = list[j];
                            list[j] = list[j + 1];
                            list[j - 1] = nTamp;
                        }

                    }
                }
                foreach (var item in list)
                {
                    Console.Write(item);
                }
            }
            catch(Exception ex) { Console.WriteLine("Error"); }
        }

        public static void Soal2()
        {
            Console.WriteLine("== Soal 2 ==");
            Console.Write("input : ");
            char[] kata = Console.ReadLine().ToLower().ToCharArray();
            int[] Aint = kata.Select(c => Convert.ToInt32(c.ToString())).ToArray();
            int genap = 0; int gajil = 0;
            foreach (var item in Aint)
            {
                if (item % 2 == 0) { genap += item; }
                else { gajil += item; }
            }
            if (genap > gajil) { Console.WriteLine(genap-gajil); }
            else { Console.WriteLine(gajil-genap); }
        }
        public static void Soal3()
        {
            Console.WriteLine("== Soal 3 ==");
            Console.Write("deret : ");
            string[] str1 = Console.ReadLine().ToLower().Split(" ");
            int[] array1 = Array.ConvertAll<String, int>(str1, int.Parse);
            int n1 = 0; int n2=0;
            for(int i=0; i<array1.Length; i++)
            {
                if (i % 4 == 0 || i<=2 || i%5==0) { n1 += array1[i]; }
                if(i%4 == 0 || i>=3 && i!=5) { n2 += array1[i]; }
            }
            Console.WriteLine($"{array1[0]} + {array1[1]} + {array1[2]} + {array1[4]} + {array1[5]} + {array1[8]} = {n1}");
            Console.WriteLine($"{array1[0]} + {array1[3]} + {array1[4]} + {array1[6]} + {array1[7]} + {array1[8]} = {n2}");
            Console.WriteLine($"{n1} + {n2} = {n1+n2}");
        }
        /* 0 1 2
           3 4 5
           6 7 8*/

        public static void Soal4()
        {
            Console.WriteLine("== Soal 4 ==");
            Console.Write("deret : ");
            char[] kata = Console.ReadLine().ToLower().ToCharArray();
            
            for(int i=0;i<kata.Length;i++)
            {
                for(int j=0; j<i; j++)
                {
                    if (kata[i] < kata[j])
                    {
                        char n = kata[i];
                        kata[i] = kata[j];
                        kata[j] = n;
                    }
                }
            }

            HashSet<char> uniqueChars = new HashSet<char>();
            string result = "";
            foreach (char c in kata)
            {
                if (!uniqueChars.Contains(c))
                {
                    uniqueChars.Add(c);
                    result += c;
                }
            }
            Console.WriteLine(result);
        }

        public static void Soal5()
        {
            Console.WriteLine("== Soal 5 ==");
            Console.Write("nilai : ");
            int nilai = int.Parse(Console.ReadLine());
            Console.Write("deret : ");
            string[] str1 = Console.ReadLine().ToLower().Split(" ");
            int[] array1 = Array.ConvertAll<String, int>(str1, int.Parse);
            List<int> list = new List<int>();

            int nCount = 0;
            for (int i = 0; i < array1.Length; i++)
            {
                for(int j=0; j<array1.Length; j++)
                {
                    for(int k=0; k < array1.Length; k++)
                    {
                        int nn = array1[i] + array1[j] + array1[k];
                        if (nn < nilai && nn > nCount) { nCount = nn; }
                    }
                }
            }
            
            Console.WriteLine(nCount);
        }
        /* 0 1 2
           3 4 5
           6 7 8*/

        public static void Soal6()
        {
            Console.WriteLine("== Soal 6 ==");
            Console.Write("Masuk : ");
            //string dd = "2023-05-01T08:10:55";
            //string dd = "2023-05-01T07:10:55";
            //string[] m1 = dd.Split('T');

            string[] m1 = Console.ReadLine().Split('T');
            string[] dMasuk = m1[0].Split("-");
            string[] tMasuk = m1[1].Split(":");
            DateTime awal = new DateTime(int.Parse(dMasuk[0]), int.Parse(dMasuk[1]), int.Parse(dMasuk[2]),
                int.Parse(tMasuk[0]), int.Parse(tMasuk[1]), int.Parse(tMasuk[2]));

            Console.Write("Keluar : ");
            //string mm = "2023-05-01T17:12:39";
            //string mm = "2023-05-02T21:12:39";
            //string[] k1 = mm.Split('T');

            string[] k1 = Console.ReadLine().Split('T');
            string[] dKeluar = k1[0].Split("-");
            string[] tKeluar = k1[1].Split(":");
            DateTime akhir = new DateTime(int.Parse(dKeluar[0]), int.Parse(dKeluar[1]), int.Parse(dKeluar[2]),
                int.Parse(tKeluar[0]), int.Parse(tKeluar[1]), int.Parse(tKeluar[2]));

            TimeSpan diff = akhir - awal;
            int totalJam = int.Parse(Math.Round(diff.TotalHours, MidpointRounding.ToPositiveInfinity).ToString());
            int Jam = int.Parse(Math.Ceiling(diff.TotalHours).ToString());
            int bayar = 0;
            while (totalJam > 0)
            {
                if (totalJam > 23) { bayar += 50000; totalJam -= 24; }
                else if (totalJam > 10) { bayar += 2000; totalJam -= 1; }
                else if (totalJam > 1) { bayar += 3000; totalJam -= 1; }
                else { bayar += 5000; totalJam -= 1; }
            }
            Console.WriteLine($"bayar : {bayar}");
        }

        public static void Soal7()
        {
            Console.WriteLine("== Soal 7 ==");
            Console.Write("X = ");
            int nX = int.Parse(Console.ReadLine());
            Console.Write("Y = ");
            int nY = int.Parse(Console.ReadLine());
            Console.Write("Z = ");
            String nZ = Console.ReadLine();
            string[] nTanggal = nZ.Split("-");
            DateTime TanggalMulai1 = new DateTime(int.Parse(nTanggal[0]), int.Parse(nTanggal[1]), int.Parse(nTanggal[2]));
            DateTime TanggalMulai2 = new DateTime(int.Parse(nTanggal[0]), int.Parse(nTanggal[1]), int.Parse(nTanggal[2]));
            TanggalMulai2 = TanggalMulai2.AddDays(nY);
            TanggalMulai1 = TanggalMulai1.AddDays(nX);
            while (true)
            {
                if (TanggalMulai1 > TanggalMulai2) { TanggalMulai2 = TanggalMulai2.AddDays(nY); }
                else if(TanggalMulai1 < TanggalMulai2) { TanggalMulai1 = TanggalMulai1.AddDays(nX); }
                else if (TanggalMulai1 == TanggalMulai2) { break; }
               
                
            }
            Console.WriteLine(TanggalMulai1.ToString("dd MMMM yyyy")+" ("+TanggalMulai2+")");
        }


        public static void Soal8()
        {
            Console.WriteLine("== Soal 8 ==");
            Console.Write("input : ");
            int nilai = int.Parse(Console.ReadLine());
            nilai--;
                int n1 = 1;
                int n2 = 1;
                while(nilai>0)
                {
                    int nTamp = n2;
                    n2 = n1 + n2;
                    n1 = nTamp;
                    nilai--;
                }
            Console.WriteLine(n1);
        }
    }
}