﻿
using System.Xml.Schema;

namespace Pretest02
{
    public class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("");
                Console.WriteLine("Pretest 2");
                Console.WriteLine("1.Soal 4");
                Console.WriteLine("2.Soal 5");
                Console.WriteLine("3.Soal 6");
                Console.WriteLine("0.exit");
                Console.Write("Pilih : ");
                int pilih = int.Parse(Console.ReadLine());
                switch (pilih)
                {
                    case 0: break;
                    case 1: Soal1(); break;
                    case 2: Soal2(); break;
                    case 3: Soal3(); break;
                    default: Console.WriteLine("Pilih 1-10"); break;
                }

                if (pilih == 0) { break; }
                else
                {
                    Console.WriteLine();
                    Console.Write("apakah anda ingin megulang Y/N ? ");
                    String pilih2 = Console.ReadLine();
                    if (pilih2.Equals("y".ToLower())) { Console.WriteLine(); continue; }
                    else { Console.WriteLine(); break; }
                }
            }
        }

        public static void Soal1()
        {
            Console.WriteLine("== Soal 4 ==");
            Console.WriteLine("Maksmal keterlambatan masing2 buku : ");
            Console.WriteLine("Maksmal Peminjaman Denda Perhari");
            Console.WriteLine("Kalkulus         5   1000");
            Console.WriteLine("Struktur Data    7   1500");
            Console.WriteLine("Matematika       4   750");
            Console.Write("Pinjam : ");
            string[] str1 = Console.ReadLine().ToLower().Replace(" ","").Split("-");
            Console.Write("Kembali : ");
            string[] str2 = Console.ReadLine().ToLower().Replace(" ", "").Split("-");
            DateTime awal = new DateTime(int.Parse(str1[0]), int.Parse(str1[1]), int.Parse(str1[2]));
            DateTime akhir = new DateTime(int.Parse(str2[0]), int.Parse(str2[1]), int.Parse(str2[2]));

            TimeSpan diff = akhir - awal;
            int telat = diff.Days;
            int total = 0;
            if(telat > 5) { int tamp = telat - 5; total+= tamp*1000; }
            if(telat > 7) { int tamp = telat - 7; total += tamp * 1500; }
            if (telat > 4) { int tamp = telat - 4; total += tamp * 750; }
            Console.WriteLine($"Hari : {telat}");
            Console.WriteLine($"Denda : {total}");
        }

        public static void Soal2()
        {
            Console.WriteLine("== Soal 5 ==");
            Console.Write("Input N = ");
            int n = int.Parse(Console.ReadLine());
            for(int i = 0; i < n; i++)
            {
                if (n % 2 == 0)
                {
                    for (int j = 0; j < n; j++)
                    {
                        if (i == n / 2 && j == n / 2 || i == n / 2 - 1 && j == n / 2 - 1 || i == n / 2 - 1 && j == n / 2 || i == n / 2 && j == n / 2 - 1)
                        { Console.Write($" {n} "); }
                        else if (i == j || i == 0 || i == n - 1 || j == n - 1 || j == 0 || j + i == n - 1) { Console.Write(" * "); }
                        else { Console.Write("   "); }

                    }
                    Console.WriteLine();
                }
                else
                {
                    for (int j = 0; j < n; j++)
                    {
                        if (i == n / 2 && j == n / 2) { Console.Write($" {n} "); }
                        else if (i == j || i == 0 || i == n - 1 || j == n - 1 || j == 0 || j + i == n - 1) { Console.Write(" * "); }
                        else { Console.Write("   "); }

                    }
                    Console.WriteLine();
                }
            }
        }
        public static void Soal3()
        {
            Console.WriteLine("== Soal 6 ==");
            Console.WriteLine("Formula perhitungan biaya parkir : ");
            Console.WriteLine("1 jam pertama        5000/jam");
            Console.WriteLine("2-7 jam              3000/jam");
            Console.WriteLine("8-12 jam             2000/jam");
            Console.WriteLine("13-23 jam            1000/jam");
            Console.WriteLine("Kelipatan 24 jam maka 20000");
            Console.WriteLine("Selanjutnya perhitungan diulang ke i jam pertama");
            Console.Write("Masuk : ");
            //string dd = "2023-05-01T08:00:00";
            //string dd = "2023-05-01T08:00:00";
            //string[] m1 = dd.Split('T');

            string[] m1 = Console.ReadLine().Split('T');
            string[] dMasuk = m1[0].Split("-");
            string[] tMasuk = m1[1].Split(":");
            DateTime awal = new DateTime(int.Parse(dMasuk[0]), int.Parse(dMasuk[1]), int.Parse(dMasuk[2]),
                int.Parse(tMasuk[0]), int.Parse(tMasuk[1]), int.Parse(tMasuk[2]));
            
            Console.Write("Keluar : ");
            //string mm = "2023-05-01T23:00:00";
            //string mm = "2023-05-02T12:00:01";
            //string[] k1 = mm.Split('T');

            string[] k1 = Console.ReadLine().Split('T');
            string[] dKeluar = k1[0].Split("-");
            string[] tKeluar = k1[1].Split(":");
            DateTime akhir = new DateTime(int.Parse(dKeluar[0]), int.Parse(dKeluar[1]), int.Parse(dKeluar[2]),
                int.Parse(tKeluar[0]), int.Parse(tKeluar[1]), int.Parse(tKeluar[2]));

            TimeSpan diff = akhir - awal;
            int totalJam = int.Parse(Math.Round(diff.TotalHours, MidpointRounding.ToPositiveInfinity).ToString());
            int Jam = int.Parse(Math.Ceiling(diff.TotalHours).ToString());
            int bayar = 0;
            while (totalJam > 0)
            {
                if (totalJam > 23) { bayar += 20000; totalJam -= 24; }
                else if (totalJam > 12) { bayar += 1000; totalJam -= 1; }
                else if (totalJam > 7) { bayar += 2000; totalJam -= 1; }
                else if (totalJam > 1) { bayar += 3000; totalJam -= 1; }
                else { bayar += 5000; totalJam -= 1; }
            }
            Console.WriteLine($"Total jam : {Jam}, bayar : {bayar}");
        }
    }
}