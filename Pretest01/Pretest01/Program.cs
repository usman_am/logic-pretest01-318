﻿using System.Linq;
using System.Net.NetworkInformation;
using static System.Net.Mime.MediaTypeNames;

namespace Pretest01
{
    public class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("");
                Console.WriteLine("Pretest 1");
                Console.WriteLine("1.Soal 1");
                Console.WriteLine("2.Soal 2");
                Console.WriteLine("3.Soal 3");
                Console.WriteLine("0.exit");
                Console.Write("Pilih : ");
                int pilih = int.Parse(Console.ReadLine());
                switch (pilih)
                {
                    case 0: break;
                    case 1: Soal1(); break;
                    case 2: Soal2(); break;
                    case 3: Soal3(); break;
                    default: Console.WriteLine("Pilih 1-10"); break;
                }

                if (pilih == 0) { break; }
                else
                {
                    Console.WriteLine();
                    Console.Write("apakah anda ingin megulang Y/N ? ");
                    String pilih2 = Console.ReadLine();
                    if (pilih2.Equals("y".ToLower())) { Console.WriteLine(); continue; }
                    else { Console.WriteLine(); break; }
                }
            }
        }

        public static void Soal1()
        {
            Console.WriteLine("== Soal 1 ==");
            Console.Write("input1 : ");
            string[] str1 = Console.ReadLine().ToLower().Split(" ");
            Console.Write("input1 : ");
            string[] str2 = Console.ReadLine().ToLower().Split(" ");
            List<int> list = new List<int>();
            for (int i=0; i<str1.Length; i++)//ctor
            {
                for(int j=0; j<str2.Length; j++)
                {
                    try
                    {
                        list.Add(int.Parse(str1[i]) + int.Parse(str2[j]));
                    }catch(Exception e)
                    {
                        continue;
                    }
                }
            }
            for(int i=0; i<list.Count; i++) {
                for(int j=0; j<i; j++)
                {
                    if (list[i] < list[j])
                    {
                        int nTamp = list[i];
                        list[i] = list[j];
                        list[j] = nTamp;
                    }
                }
            }
            Console.WriteLine($"{list[0]} {list[list.Count-1]}");
        }

        public static void Soal2()
        {
            Console.WriteLine("== Soal 2 ==");
            Console.Write("input1 : ");
            string str = Console.ReadLine().ToLower();
            string[] strArray1 = str.Select(x => x.ToString()).ToArray();
            Console.Write("input1 : ");
            string str2 = Console.ReadLine().Replace(" ","").ToLower();
            string[] strArray2 = str2.Select(x => x.ToString()).ToArray();
            int ir = 0;
            for (int i = 0; i < strArray1.Length; i++)
            {
                int nTamp = 0;
                for (int j=0;  j < strArray2.Length; j++)
                {
                    if (strArray1[i].Contains(strArray2[j])){
                        nTamp++; break;
                    }
                }
                if (nTamp > 0) { ir++; }
            }
            if (ir == str.Length)
            {
                Console.WriteLine("YES");
            }
            else { Console.WriteLine("NO"); }

        }
        public static void Soal3()
        {
            Console.WriteLine("== Soal 3 ==");
            Console.Write("Words : ");
            char[] text = Console.ReadLine().ToCharArray();

            string patokan = "abcdefghijklmnopqrstuvwxyz0123456789";

            for (int i = 1; i < text.Length; i++)
            {
                for (int j = i; j > i; j--)
                {
                    if (patokan.IndexOf(text[j]) < patokan.IndexOf(text[j - 1]))
                    {
                        var nTamp = text[j];
                        text[j] = text[j - 1];
                        text[j - 1] = nTamp;
                    }
                }
            }
            foreach (var item in text)
            {
                Console.Write($"{item}");
            }
            //0:47 9:57 a:97 z:122
            /* String n1 = "";
             String n2 = "";
             foreach (char item in text)
             {
                 if (item > 47 && item < 59) { n1 += item; }
                 else if(item > 95 && item < 124) { n2 += item; }

             }
             //Console.Write($"{item}:{item + 0} ");
             Console.WriteLine($"{n2}{n1}");
         }*/
        }
    }
}